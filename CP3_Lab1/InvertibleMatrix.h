/*
 * Protects the file from being included 
 * many times, alternatively can be replaced
 * with so called include guards, being
 * #ifndef <some_expresion>
 * #define <the_same_expression_as_above>
 * ...code...
 * #endif				// at the end of file
 */
#pragma once

/* Class declaration using inheritance */
class InvertibleMatrix : public SquareMatrix
{
public:
	/* Destructor */
	~InvertibleMatrix() {}
	/* Constructor with parameter */
	InvertibleMatrix(int size);
	/* Matrix transposition */
	Matrix* transpose();
	/* Matrix inverse */
	InvertibleMatrix* invertMatrix();
};