/*
 * Protects the file from being included 
 * many times, alternatively can be replaced
 * with so called include guards, being
 * #ifndef <some_expresion>
 * #define <the_same_expression_as_above>
 * ...code...
 * #endif				// at the end of file
 */
#pragma once

/* Class declaration using inheritance */
class SquareMatrix : public Matrix
{
	/* Finds determinant of 2 by 2 matrix */
	float find2By2Determinant(SquareMatrix *matrix);
	/* 
	 * Finds determinant of 3 by 3 matrix 
	 * using Sarrus mathod
	 */
	float find3By3Determinant(SquareMatrix *matrix);
protected:
	/* Creates minor from given matrix without row & col */
	SquareMatrix* rewriteData(SquareMatrix *matrix, int row, int col);
public:
	/* Destructor */
	~SquareMatrix() {}
	/* Constructor with parameter */
	SquareMatrix(int size);
	/* Matrix transposition */
	Matrix* transpose();
	/* Finds determinant of any square matrix */
	float findDeterminant(SquareMatrix *matrix);	
};