#include "common_headers.h"

using namespace std;

/*
 * Constructor with parameter
 * @param size		-		matrix width and height at the same time
 */
InvertibleMatrix::InvertibleMatrix(int size) : SquareMatrix(size)
{
	while(((size >= 1) && (size <= 8)) && (findDeterminant(this) == 0.0f))
		generateData();
}

/*
 * Matrix transposition
 * @return			-		transposed matrix
 */
Matrix* InvertibleMatrix::transpose()
{
	InvertibleMatrix *matrix = new InvertibleMatrix(width);

	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			matrix->data[j][i] = data[i][j];
	return matrix;
}

/*
 * Matrix inverse computed using minors
 * @return			-		inverse matrix
 */
InvertibleMatrix* InvertibleMatrix::invertMatrix()
{
	SquareMatrix *tempSq;
	float determinant = findDeterminant(this);
	Matrix* minorMatrix = new InvertibleMatrix(width);
	InvertibleMatrix* matrix = new InvertibleMatrix(width);
	
	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
		{
			tempSq = rewriteData(this, i, j);
			reinterpret_cast<InvertibleMatrix*>(minorMatrix)->data[i][j] = powf(-1.0f, (float) i + j) * findDeterminant(tempSq);
			delete tempSq;
		}
	matrix = dynamic_cast<InvertibleMatrix*>(minorMatrix->transpose());
	delete minorMatrix;
	(*matrix) *= (1.0f / determinant);
	return matrix;
}