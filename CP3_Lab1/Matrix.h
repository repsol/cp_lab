/*
 * Protects the file from being included 
 * many times, alternatively can be replaced
 * with so called include guards, being
 * #ifndef <some_expresion>
 * #define <the_same_expression_as_above>
 * ...code...
 * #endif				// at the end of file
 */
#pragma once

/* Class declaration */
class Matrix
{
protected:
	/* 
	 * Two-dimensional array representing
	 * matrix contents
	 */
	float **data;
	/* Matrix width and height */
	int width, height;
	/* Maximum range of matrix values */
	const int MAX_VALUE;
	/* Method generating matrix contents */
	void generateData();
public:
	/* Destructor */
	virtual ~Matrix();	
	/* Copy constructor */
	Matrix(const Matrix& matrix);
	/* Constructor with parameters */
	Matrix(int width, int height);
	/* Overloaded assignment operator */
	Matrix& operator=(const Matrix& matrix);
	/* Overloaded *= operator for matrix-scalar multiplication */
	Matrix& operator*=(const float scalar);
	/* Overloaded output stream operator */
	friend std::ostream& operator<<(std::ostream& o, const Matrix& matrix);
	/* Matrix transposition */
	virtual Matrix* transpose();
	/* Inline accessor methods (getters & setters) */
	int getWidth() 
	{ return width; }
	int getHeight()
	{ return height; }
	void setWidth(int width)
	{ this->width = width; }
	void setHeight(int height)
	{ this->height = height; }
};