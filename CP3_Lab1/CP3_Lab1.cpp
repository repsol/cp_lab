#include "common_headers.h"

using namespace std;

int main(int argc, char* argv[])
{
	// Sets the seed of random generator
	srand(time(NULL));	
	
	SquareMatrix *sqMatrix;
	InvertibleMatrix *invMatrix;
	Matrix *matrix1 = new Matrix(3, 5);
	Matrix *matrix2 = new SquareMatrix(4);
	Matrix *matrix3 = new InvertibleMatrix(4), *matrix4;
	
	cout << "Matrix: " << endl;
	cout << *matrix1 << endl;
	cout << "SquareMatrix: " << endl;
	cout << *matrix2 << endl;
	cout << "InvertibleMatrix: " << endl;
	cout << *matrix3 << endl;
	cout << "Inverse matrix: " << endl;
	sqMatrix = dynamic_cast<SquareMatrix*>(matrix2);
	invMatrix = dynamic_cast<InvertibleMatrix*>(matrix3);
	matrix4 = invMatrix->invertMatrix();
	cout << *matrix4 << endl;
	cout << "Determinant of SquareMatrix: " << sqMatrix->findDeterminant(sqMatrix) << endl;
	cout << "Determinant of InvertibleMatrix: " << invMatrix->findDeterminant(invMatrix) << endl;	
	delete matrix1;
	delete matrix2;
	delete matrix3;
	delete matrix4;
	system("pause");
	return 0;
}

