#include "common_headers.h"

using namespace std;

/*
 * Generates matrix contents using rand() 
 * function in the range [0, MAX_VALUE - 1]
 */
void Matrix::generateData()
{
	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			data[i][j] = rand() % MAX_VALUE;
}

/* 
 * Destructor - frees dynamically allocated
 * memory
 */
Matrix::~Matrix()
{
	for(int i = 0; i < height; i++)
		delete [] data[i];
	delete [] data;
}

/* 
 * Copy constructor 
 * @param matrix	-		existing object whose contents 
 *							are to be copied
 */
Matrix::Matrix(const Matrix& matrix) : MAX_VALUE(10)
{
	// Copies data from existing object
	width = matrix.width;
	height = matrix.height;
	data = new float*[height];
	for(int i = 0; i < height; i++)
		data[i] = new float[width];
	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			data[i][j] = matrix.data[i][j];
}
	
/* 
 * Constructor with parameters
 * @param width		-		matrix width
 * @param height	-		matrix height
 */
Matrix::Matrix(int width, int height) : MAX_VALUE(10), width(width), height(height)
{
	data = new float*[height];
	for(int i = 0; i < height; i++)
		data[i] = new float[width];
	// Generates matrix contents
	generateData();
}

/* 
 * Overloaded assignment operator 
 * @param matrix	-		existing object whose contents 
 *							are to be copied
 * @return			-		copy of existing object
 */
Matrix& Matrix::operator=(const Matrix& matrix)
{
	if(this != &matrix)
	{
		// Frees already allocated memory
		for(int i = 0; i < height; i++)
			delete [] data[i];
		delete [] data;
		// Copies data from existing object
		width = matrix.width;
		height = matrix.height;
		data = new float*[height];
		for(int i = 0; i < height; i++)
			data[i] = new float[width];
		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
				data[i][j] = matrix.data[i][j];
	}
	return *this;
}

/* 
 * Overloaded *= operator for matrix-scalar multiplication 
 * @param scalar	-		scalar to multiply the matrix by
 * @return			-		multiplied matrix
 */
Matrix& Matrix::operator*=(const float scalar)
{
	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			data[i][j] *= scalar;
	return *this;
}

/* 
 * Overloaded output stream operator 
 * @param o			-		output stream, e.g. cout, cerr
 * @param matrix	-		object to be printed
 * @return			-		output stream filled with data
 */
ostream& operator<<(ostream& o, const Matrix& matrix)
{
	
	for(int i = 0; i < matrix.height; i++)
	{			
		for(int j = 0; j < matrix.width; j++)
			/*
			 * setw(...) - sets minimum number of digits printed
			 * setprecision(...) - sets number of digits after decimal dot
			 * fixed - denotes fixed-point float values, i.e. with certain
			 *		   number of digits after decimal dot
			 */
			o << setw(5) << setprecision(2) << fixed << matrix.data[i][j] << " ";
		o << "\n";
	}
	return o;
}

/* 
 * Matrix transposition 
 * @return			-		transposed matrix
 */
Matrix* Matrix::transpose()
{
	Matrix *matrix = new Matrix(height, width);

	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			matrix->data[j][i] = data[i][j];
	return matrix;
}