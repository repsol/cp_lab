#include "common_headers.h"

using namespace std;

/*
 * Finds determinant of 2 x 2 matrix
 * @param matrix	-		matrix whose determinant is to be found
 * @return			-		matrix determinant
 */
float SquareMatrix::find2By2Determinant(SquareMatrix *matrix)
{
	return matrix->data[0][0] * matrix->data[1][1] - matrix->data[0][1] * matrix->data[1][0];
}

/*
 * Finds determinant of 3 x 3 matrix using Sarrus method
 * @param matrix	-		matrix whose determinant is to be found
 * @return			-		matrix determinant
 */
float SquareMatrix::find3By3Determinant(SquareMatrix *matrix)
{
	return matrix->data[0][0] * matrix->data[1][1] * matrix->data[2][2] + 
		   matrix->data[1][0] * matrix->data[2][1] * matrix->data[0][2] +
		   matrix->data[2][0] * matrix->data[0][1] * matrix->data[1][2] -
		   matrix->data[2][0] * matrix->data[1][1] * matrix->data[0][2] - 
		   matrix->data[0][0] * matrix->data[2][1] * matrix->data[1][2] -
		   matrix->data[1][0] * matrix->data[0][1] * matrix->data[2][2];
}

/*
 * Creates minor out of given matrix without given row and column
 * @param matrix	-		matrix whose minor is to be found
 * @param row		-		row to be excluded from input matrix
 * @param col		-		column to be excluded from input matrix
 * @return			-		minor
 */
SquareMatrix* SquareMatrix::rewriteData(SquareMatrix *matrix, int row, int col)
{
	int k, l;
	/*
	 * Dynamic memory allocation - remember 
	 * about freeing the memory later!
	 */
	SquareMatrix *sq = new SquareMatrix(matrix->width - 1);
	
	for(int i = 0; i < matrix->height; i++)
		if(i != row)
		{
			/*
			 * Remember about changing indices properly
			 * not to go out of array
			 */
			k = (i > row ? (i - 1) : i);
			for(int j = 0; j < matrix->width; j++)
				if(j != col)
				{
					/*
					 * Remember about changing indices properly
					 * not to go out of array
					 */
					l = (j > col ? (j - 1) : j);
					sq->data[k][l] = matrix->data[i][j];			
				}
		}
	return sq;
}

/*
 * Constructor with parameter
 * @param size		-		matrix width and height at the same time
 */
SquareMatrix::SquareMatrix(int size) : Matrix(size, size) {}

/*
 * Matrix transposition
 * @return			-		transposed matrix
 */
Matrix* SquareMatrix::transpose()
{
	SquareMatrix *matrix = new SquareMatrix(width);

	for(int i = 0; i < height; i++)
		for(int j = 0; j < width; j++)
			matrix->data[j][i] = data[i][j];
	return matrix;
}

/*
 * Finds the determinant of matrix of arbitrary size
 * of up to 8 x 8
 * @param matrix	-		matrix to be transposed
 * @return			-		matrix determinant
 */
float SquareMatrix::findDeterminant(SquareMatrix *matrix)
{
	SquareMatrix *tempSq;		
	float determinant = 0.0f;	

	switch(matrix->width)
	{
	case 1:
		return data[0][0];
	case 2:
		return find2By2Determinant(matrix);
	case 3:
		return find3By3Determinant(matrix);
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
		for(int i = 0; i < matrix->width; i++)
		{
			tempSq = rewriteData(matrix, 0, i);
			determinant += powf(-1.0f, (float) i) * matrix->data[0][i] * findDeterminant(tempSq);
			delete tempSq;
		}
		return determinant;
	default:	
		/* 
		 * Slightly dangerous solution since 0 may also be
		 * just a result of determinant's calculation
		 */
		return 0.0f;	
	}
}