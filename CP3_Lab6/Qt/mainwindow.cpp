#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonGenerate_clicked()
{
    int min, max, count;
    bool b, unique;
    int tmp;

    unique = ui->checkBoxUnique->isChecked();
    min = ui->lineEditMin->text().toInt(&b);
    if (!b)
    {
        QMessageBox::warning(this, "Error", "Minimum");
        return;
    }
    max = ui->lineEditMax->text().toInt(&b);
    if (!b)
    {
        QMessageBox::warning(this, "Error", "Maximum");
        return;
    }
    count = ui->lineEditCount->text().toInt(&b);
    if (!b)
    {
        QMessageBox::warning(this, "Error", "Count");
        return;
    }
    if (min > max)
    {
        QMessageBox::warning(this, "Error", "Minimum > Maximum");
        return;
    }
    if (count <= 0)
    {
        QMessageBox::warning(this, "Error", "Count <= 0");
        return;
    }
    if (unique && count > max-min+1)
    {
        QMessageBox::warning(this, "Error", "count > max-min+1, unique");
        return;
    }

    v.clear();
    ui->textEditOut->clear();

    for (int i=0; v.count()<count; ++i)
    {
        tmp = min + qrand()%(max-min+1);
        if (unique && v.contains(tmp))
            continue;

        v.push_back(tmp);
        ui->textEditOut->append(QString::number(v.last()));

        if (v.count()==count)
            ui->textEditOut->append("Iterations: "+QString::number(i+1));
    }
}
