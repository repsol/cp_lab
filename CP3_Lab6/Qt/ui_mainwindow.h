/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed 14. Nov 10:19:20 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEditMax;
    QLabel *label_3;
    QCheckBox *checkBoxUnique;
    QPushButton *pushButtonGenerate;
    QTextEdit *textEditOut;
    QLineEdit *lineEditCount;
    QLineEdit *lineEditMin;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(155, 291);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        formLayout = new QFormLayout(centralWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        lineEditMax = new QLineEdit(centralWidget);
        lineEditMax->setObjectName(QString::fromUtf8("lineEditMax"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lineEditMax);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        checkBoxUnique = new QCheckBox(centralWidget);
        checkBoxUnique->setObjectName(QString::fromUtf8("checkBoxUnique"));

        formLayout->setWidget(4, QFormLayout::LabelRole, checkBoxUnique);

        pushButtonGenerate = new QPushButton(centralWidget);
        pushButtonGenerate->setObjectName(QString::fromUtf8("pushButtonGenerate"));

        formLayout->setWidget(4, QFormLayout::FieldRole, pushButtonGenerate);

        textEditOut = new QTextEdit(centralWidget);
        textEditOut->setObjectName(QString::fromUtf8("textEditOut"));
        textEditOut->setReadOnly(true);

        formLayout->setWidget(5, QFormLayout::SpanningRole, textEditOut);

        lineEditCount = new QLineEdit(centralWidget);
        lineEditCount->setObjectName(QString::fromUtf8("lineEditCount"));

        formLayout->setWidget(3, QFormLayout::FieldRole, lineEditCount);

        lineEditMin = new QLineEdit(centralWidget);
        lineEditMin->setObjectName(QString::fromUtf8("lineEditMin"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEditMin);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 155, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Random generator", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Minimum", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Maximum", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Count", 0, QApplication::UnicodeUTF8));
        checkBoxUnique->setText(QApplication::translate("MainWindow", "Unique", 0, QApplication::UnicodeUTF8));
        pushButtonGenerate->setText(QApplication::translate("MainWindow", "Generate", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
