#pragma once

namespace CP3_Lab6 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::Button^  buttonGenerate;
    protected: 

    private: System::Windows::Forms::CheckBox^  checkBoxUnique;
    protected: 

    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::TextBox^  textBoxMin;
    private: System::Windows::Forms::TextBox^  textBoxMax;
    private: System::Windows::Forms::TextBox^  textBoxCount;





	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;
    private: System::Windows::Forms::TextBox^  textBoxOut;

             System::Int32 *numbers;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
            this->buttonGenerate = (gcnew System::Windows::Forms::Button());
            this->checkBoxUnique = (gcnew System::Windows::Forms::CheckBox());
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->label2 = (gcnew System::Windows::Forms::Label());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->textBoxMin = (gcnew System::Windows::Forms::TextBox());
            this->textBoxMax = (gcnew System::Windows::Forms::TextBox());
            this->textBoxCount = (gcnew System::Windows::Forms::TextBox());
            this->textBoxOut = (gcnew System::Windows::Forms::TextBox());
            this->SuspendLayout();
            // 
            // buttonGenerate
            // 
            this->buttonGenerate->Location = System::Drawing::Point(123, 121);
            this->buttonGenerate->Name = L"buttonGenerate";
            this->buttonGenerate->Size = System::Drawing::Size(75, 23);
            this->buttonGenerate->TabIndex = 0;
            this->buttonGenerate->Text = L"Generate";
            this->buttonGenerate->UseVisualStyleBackColor = true;
            this->buttonGenerate->Click += gcnew System::EventHandler(this, &Form1::buttonGenerate_Click);
            // 
            // checkBoxUnique
            // 
            this->checkBoxUnique->AutoSize = true;
            this->checkBoxUnique->Location = System::Drawing::Point(29, 125);
            this->checkBoxUnique->Name = L"checkBoxUnique";
            this->checkBoxUnique->Size = System::Drawing::Size(58, 17);
            this->checkBoxUnique->TabIndex = 1;
            this->checkBoxUnique->Text = L"unique";
            this->checkBoxUnique->UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->Location = System::Drawing::Point(26, 27);
            this->label1->Name = L"label1";
            this->label1->Size = System::Drawing::Size(47, 13);
            this->label1->TabIndex = 2;
            this->label1->Text = L"minimum";
            // 
            // label2
            // 
            this->label2->AutoSize = true;
            this->label2->Location = System::Drawing::Point(26, 54);
            this->label2->Name = L"label2";
            this->label2->Size = System::Drawing::Size(50, 13);
            this->label2->TabIndex = 3;
            this->label2->Text = L"maximum";
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Location = System::Drawing::Point(26, 81);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(34, 13);
            this->label3->TabIndex = 4;
            this->label3->Text = L"count";
            this->label3->Click += gcnew System::EventHandler(this, &Form1::label3_Click);
            // 
            // textBoxMin
            // 
            this->textBoxMin->Location = System::Drawing::Point(98, 24);
            this->textBoxMin->Name = L"textBoxMin";
            this->textBoxMin->Size = System::Drawing::Size(100, 20);
            this->textBoxMin->TabIndex = 5;
            // 
            // textBoxMax
            // 
            this->textBoxMax->Location = System::Drawing::Point(98, 51);
            this->textBoxMax->Name = L"textBoxMax";
            this->textBoxMax->Size = System::Drawing::Size(100, 20);
            this->textBoxMax->TabIndex = 6;
            // 
            // textBoxCount
            // 
            this->textBoxCount->Location = System::Drawing::Point(98, 78);
            this->textBoxCount->Name = L"textBoxCount";
            this->textBoxCount->Size = System::Drawing::Size(100, 20);
            this->textBoxCount->TabIndex = 7;
            // 
            // textBoxOut
            // 
            this->textBoxOut->AcceptsReturn = true;
            this->textBoxOut->Location = System::Drawing::Point(29, 160);
            this->textBoxOut->Multiline = true;
            this->textBoxOut->Name = L"textBoxOut";
            this->textBoxOut->ReadOnly = true;
            this->textBoxOut->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
            this->textBoxOut->Size = System::Drawing::Size(169, 136);
            this->textBoxOut->TabIndex = 8;
            // 
            // Form1
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(225, 308);
            this->Controls->Add(this->textBoxOut);
            this->Controls->Add(this->textBoxCount);
            this->Controls->Add(this->textBoxMax);
            this->Controls->Add(this->textBoxMin);
            this->Controls->Add(this->label3);
            this->Controls->Add(this->label2);
            this->Controls->Add(this->label1);
            this->Controls->Add(this->checkBoxUnique);
            this->Controls->Add(this->buttonGenerate);
            this->Name = L"Form1";
            this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
            this->Text = L"WindowsApp";
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
private: System::Void label3_Click(System::Object^  sender, System::EventArgs^  e) {
             }

private: System::Void richTextBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
         }

private: System::Void buttonGenerate_Click(System::Object^  sender, System::EventArgs^  e) {
             int min, max, count;
             bool unique = checkBoxUnique->Checked;

             if (!System::String::IsNullOrEmpty(textBoxMin->Text))
                 min = System::Convert::ToInt32(textBoxMin->Text);
             else
             {
                 System::Windows::Forms::MessageBox::Show("Empty field!", "Error");
                 return;
             }
             if (!System::String::IsNullOrEmpty(textBoxMax->Text))
                 max = System::Convert::ToInt32(textBoxMax->Text);
             else
             {
                 System::Windows::Forms::MessageBox::Show("Empty field!", "Error");
                 return;
             }
             if (!System::String::IsNullOrEmpty(textBoxCount->Text))
                 count = System::Convert::ToInt32(textBoxCount->Text);
             else
             {
                 System::Windows::Forms::MessageBox::Show("Empty field!", "Error");
                 return;
             }
             if (min > max)
             {
                 System::Windows::Forms::MessageBox::Show("Min > Max", "Error");
                 return;
             }
             if (count <= 0)
             {
                 System::Windows::Forms::MessageBox::Show("Count must be > 0", "Error");
                 return;
             }
             if (unique && count > max-min+1)
             {
                 System::Windows::Forms::MessageBox::Show("Count too large for unique numbers.", "Error");
                 return;
             }

             DateTime t;
             Random r(t.Now.ToBinary());

             textBoxOut->Clear();
             numbers = new int [count];
             int tmp;
             /* Krzysiu method
             bool notFoundAll = true;
             bool isRepited = false;
             int numbersCount=0;
             
             numbers[0] = min + r.Next() % (max-min+1);
             numbersCount++;

             while(notFoundAll){
                tmp = min + r.Next() % (max-min+1);
                if(unique){
                for(int i=0; i<numbersCount; i++){
                    if(numbers[i] == tmp) {
                        isRepited = true;
                        break;
                    }
                }
                }
                if(!isRepited) 
                {
                    numbers[numbersCount++] = tmp;
                    isRepited = false;
                }
                isRepited = false;
                if(numbersCount == count) notFoundAll = false;
             }

             for(int i=0; i<numbersCount; i++){
                textBoxOut->Text += numbers[i].ToString()+" ";
             }*/
             
             for (int i=0; i < count; ++i)
             {
                 tmp = min + r.Next() % (max-min+1);
                 numbers[i] = tmp;
                 if (unique)
                     for (int j = 0; j < i; ++j)
                         if (numbers[j] == tmp)
                         {
                             --i;
                             break;
                         }
             }

             for (int i=0; i<count; ++i)
                 textBoxOut->Text += numbers[i].ToString()+" ";
             
             delete [] numbers;
         }
};
}

