#include "ProgrammerException.h"
#include <sstream>

ProgrammerException::ProgrammerException(std::string msgClass, std::string msg, int line) : exception(), messageClass(msgClass), message(msg), codeLine(line)
{
}

ProgrammerException::~ProgrammerException()
{
}

const char * ProgrammerException::what() const
{
    std::stringstream s;
    s<<"["<<messageClass<<":"<<codeLine<<"]:"<<message<<std::endl;
    whatText = s.str();
    return whatText.c_str();
}