#ifndef PROGRAMMER_EXCEPTION_H_
#define PROGRAMMER_EXCEPTION_H_

#include <exception>
#include <string>

class ProgrammerException: public std::exception {
private:
    std::string messageClass;
    std::string message;
    int codeLine;
    mutable std::string whatText;

public:
    ProgrammerException(std::string msgClass, std::string msg, int line);
    virtual ~ProgrammerException();

    void setMessageClass(const std::string msgClass)
    {
        messageClass = msgClass;
    }
    void setMessage(const std::string msg)
    {
        message = msg;
    }
    void setCodeLine(const int line)
    {
        codeLine = line;
    }

    std::string getMessageClass() const
    {
        return messageClass;
    }
    std::string getMessage() const
    {
        return message;
    }
    int getCodeLine() const
    {
        return codeLine;
    }

    const char * what() const;
};

#endif /* PROGRAMMER_EXCEPTION_H_ */