//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//    float a=0, b=0;
//    cout<<"Type a and b: ";
//    cin>>a>>b;
//    try
//    {
//        if (b==0) throw 0;
//        float c = a/b;
//        cout<<a<<"/"<<b<<" = "<<c<<endl;
//    }
//    catch (...)
//    {
//        cout<<"Error! b cannot be 0.\n";
//        
//        // task 2
//        // throw exception in catch block does not cause infinite recursion.
//        //throw 0;
//    }
//    system("pause");
//    return 0;
//}