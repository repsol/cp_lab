#include "List.h"
#include "ProgrammerException.h"
#include <iostream>

List::List(std::string name) : name(name), size(0), elements(0) {
}

List::~List() {
	clean();
}

void List::add(Person* person) {

    if (person == 0)
        throw ProgrammerException("List.cpp", " Invalid person.", __LINE__);

    ListItem* item;
    try {
        item = new ListItem(person);
    }
    catch (ProgrammerException& e)
    {
        e.setMessageClass(e.getMessageClass()+" List.cpp");
        throw e;
    }

	if (size == 0) {
		this->elements = item;
	} else {
		ListItem* temp = this->elements;
		while (temp->getNext() != 0) {
			temp = temp->getNext();
		}
		temp->setNext(item);
	}
	size++;
}

void List::clean(){

	while (size > 0) {
		remove(size-1);
	}
}

void List::remove(int i) {

    if (i < 0 || i >= size)
        throw ProgrammerException("List.cpp", "Index out of range.", __LINE__);
	
    ListItem* deletedItem = 0;
	if (i == 0) {
		deletedItem = this->elements;
		this->elements = this->elements->getNext();

	} else {
		ListItem* temp = this->elements;
		int current = 1;
		while (current < i) {
			temp = temp->getNext();
			current++;
		}

		deletedItem = temp->getNext();
		temp->setNext(deletedItem->getNext());
	}

	delete deletedItem;
	size--;
}

Person* List::get(int i) const {

	if (i < 0 || i >= size)
        throw ProgrammerException("List.cpp", "Index out of range.", __LINE__);
    
    Person* person = 0;

	if (i == 0) {
			person = this->elements->getPerson();
	} else {
		ListItem* temp = this->elements;
		int current = 0;
		while (current < i) {
			temp = temp->getNext();
			current++;
		}

		person = temp->getPerson();
	}

	return person;
}

void List::print() {
	std::cout << "LIST '" << this->name << "' (size: " << this->size << ")" << std::endl;
	ListItem* temp = this->elements;
	while (temp != 0) {
		temp->print();
		temp = temp->getNext();
	}
	std::cout << "-----------------------------------" << std::endl << std::endl;
}
