#include "ListItem.h"
#include "ProgrammerException.h"

#include <iostream>

ListItem::ListItem(Person* person) : next(0), person(person) {
    try {
        this->person->validate();
    }
    catch (ProgrammerException& e)
    {
        e.setMessageClass(e.getMessageClass()+" ListItem.cpp");
        throw e;
    }
}

ListItem::~ListItem() {
}

void ListItem::print() {
	Person* person = this->getPerson();
    if (person == 0)
        throw ProgrammerException("ListItem.cpp", "print function - null pointer to person.", __LINE__);
	std::cout << person->getName() << " " << person->getSurname() << "\t PESEL: " << person->getPesel() << std::endl;
}

