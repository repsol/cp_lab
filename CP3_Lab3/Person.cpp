#include "Person.h"
#include "ProgrammerException.h"

Person::Person(std::string name, std::string surname, std::string pesel): name(name), surname(surname), pesel(pesel) {
    //validate();
}

Person::~Person() {
}

void Person::validate()
{
    ProgrammerException exception("Person.cpp", "", 10);

    if (name == "")
        exception.setMessage(exception.getMessage()+" Invalid name.");
    if (surname == "")
        exception.setMessage(exception.getMessage()+" Invalid surname.");
    if (pesel.size() != 11)
        exception.setMessage(exception.getMessage()+" Invalid PESEL.");

    if (exception.getMessage() != "")
        throw exception;
}