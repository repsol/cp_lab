#ifndef LISTITEM_H_
#define LISTITEM_H_

#include "Person.h"

class ListItem {
private:
	Person* person;
	ListItem* next;

public:
	ListItem(Person *person);
	virtual ~ListItem();

	void print();

	Person* getPerson() const {
		return person;
	}

	void setPerson(Person* person) {
		this->person = person;
	}

	ListItem* getNext() const {
		return next;
	}

	void setNext(ListItem* next) {
		this->next = next;
	}
};

#endif /* LISTITEM_H_ */
