//============================================================================
// Name        : main.cpp
// Author      : Tomasz Fio�ka
// Version     : 1
// Copyright   : 2012
// Description : Exceptions in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Person.h"
#include "List.h"
#include "ProgrammerException.h"

int main() {

	List* myList = new List("myList");

    try {
	//Add some persons
	myList->add(new Person("Jan","Kowalski","00000000000"));
	myList->add(new Person("Pawel","Nowak","00000000001"));
	myList->add(new Person("Adrian","Koder","00000000002"));
	myList->add(new Person("Piotr","Ziolko","00000000003"));
    myList->add(new Person("","","00000000"));
    }
    catch (exception& e)
    {
        cout<<e.what()<<endl;
    }
    catch (...)
    {
        cout<<"\nException handling after add some people to list.\n";
    }
    myList->print();

    try {
	//remove single person
	myList->remove(5);
    }
    catch (exception& e)
    {
        cout<<e.what()<<endl;
    }
    catch (...)
    {
        cout<<"\nException handling after remove person from list.\n";
    }
    myList->print();

    try {
	//get single person
	Person* p = myList->get(-1);
	cout << p->getName() << std::endl<< std::endl;
    }
    catch (exception& e)
    {
        cout<<e.what()<<endl;
    }
    catch (...)
    {
        cout<<"Exception handling after get() function.\n";
    }

	//clean list
	myList->clean();
	myList->print();

    system("pause");
	return 0;
}
