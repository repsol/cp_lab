#include "common_headers.h"

const int SIZE = 4;

int main(int argc, char* argv[])
{
	/*
	 * If all the classes are implemented 
	 * properly, the result of print() method call
	 * should produce the following line:
	 * WorkingStudent: John Smith, aged 25, with average 4.75, working in XXXCompany earns 2500$ per month
	 */
	float grades[] = {4.0, 5.0, 5.0, 5.0};
	Person *ws = new WorkingStudent("John", "Smith", 25, grades, SIZE, "XXXCompany", 2500);

	/*
	 * This line should produce the following output:
	 * WorkingStudent John Smith, aged 25, with average 4.75, working in XXXCompany earns 2500$ per month
	 */
	ws->print();
	/*
	 * This line of code should result in
	 * complete memory deallocation - remember
	 * about proper destructor definitions
	 */	
	delete ws;
	system("pause");
	return 0;
}