#include "common_headers.h"

using namespace std;

Student::Student(string name, string surname, int age, float *grades, int size): Person(name, surname, age), size(size)
{
	this->grades = new float[size];
	for (int i=0; i<size; ++i)
		this->grades[i] = grades[i];
}

Student::~Student()
{
	delete [] grades;
}

int Student::getSize()
{
	return size;
}

float * Student::getGrades()
{
	return grades;
}

void Student::setGrades(float *grades, int size)
{
	if (this->grades)
		delete [] this->grades;

	this->grades = new float[size];
	for (int i=0; i<size; ++i)
		this->grades[i] = grades[i];
	this->size = size;
}

float Student::calculateAvg()
{
	float result = 0;
	for (int i=0; i<size; ++i)
		result += grades[i];
	result /= size;

	return result;
}

void Student::print()
{
	cout<<"Student "<<name<<" "<<surname<<", aged "<<age<<" with average "<<calculateAvg()<<".";
}