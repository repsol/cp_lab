#pragma once

class Student: virtual public Person
{
private:
	int size;

protected:
	float *grades;

// Methods
protected:
	float calculateAvg();

public:
	Student(string name, string surname, int age, float *grades, int size);
	~Student();

	void print();
	int getSize();
	float * getGrades();

	void setGrades(float *grades, int size);
};