#include "common_headers.h"

using namespace std;

int Person::getAge(){
	return this->age;
}

void Person::setAge(int age){
	this->age = age;
}

string Person::getName(){
	return this->name;
}

void Person::setName(string name){
	this->name = name;
}

string Person::getSurname(){
	return this->surname;
}

void Person::setSurname(string surname){
	this->surname;
}

void Person::print(){
	cout << "Person: ";
	cout << this->name;
	cout << " ";
	cout << this->surname;
	cout << ", ";
	cout << "aged ";
	cout << this->age;
}

Person::Person(string name, string surname, int age){
	this->name = name;
	this->surname = surname;
	this->age = age;
}

Person::~Person(){}