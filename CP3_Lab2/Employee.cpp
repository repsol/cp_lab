#include "common_headers.h"

using namespace std;

void Employee::setComapanyName(string companyName){
	this->companyName = companyName;
}

string Employee::getCompany(){
	return this->companyName;
}

void Employee::setSalary(int salary){
	this->salary = salary;
}

int Employee::getSalary(){
	return this->salary;
}

void Employee::print(){
	cout << "Employee: ";
	cout << this->name;
	cout << " ";
	cout << this->surname;
	cout << ", ";
	cout << "aged ";
	cout << this->age;
	cout << "working in ";
	cout << this->companyName;
	cout << " earns ";
	cout << this->getSalary();
	cout << "$ per month";
}

Employee::Employee(string name, string surname, int age, string companyName, int salary):Person(name, surname, age){
	this->companyName = companyName;
	this->salary = salary;
}

Employee::~Employee(){
}