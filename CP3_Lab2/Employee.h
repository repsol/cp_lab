#pragma once

using namespace std;
class Employee : virtual public Person{
	protected:
		string companyName;
		int salary;

	public:
		void setComapanyName(string companyName);
		string getCompany();
		void setSalary(int salary);
		int getSalary();
		void print();
		Employee(string name, string surname, int age, string companyName, int salary);
		~Employee();
};