#pragma once

using namespace std;
class Person {
	protected:
		string name;
		string surname;
		int age;
	public:
		int getAge();
		void setAge(int age);
		string getName();
		void setName(string name);
		string getSurname();
		void setSurname(string surname);
		virtual void print();

		Person(string name, string surname, int age);
		virtual ~Person();
};