#pragma once

class WorkingStudent: public Student, public Employee
{
private:
	bool hasScholarship;

// Methods
private:
	void calculateScholarship();

public:
	WorkingStudent(string name, string surname, int age, float *grades, int size, string companyName, int salary);
	~WorkingStudent();

	void print();
	bool getSchorarship();
};