#include "common_headers.h"

using namespace std;

WorkingStudent::WorkingStudent(string name, string surname, int age, float *grades, int size, string companyName, int salary): Person(name, surname, age), Student(name, surname, age, grades, size), Employee(name, surname, age, companyName, salary)
{
	hasScholarship = false;
}

WorkingStudent::~WorkingStudent()
{
}

bool WorkingStudent::getSchorarship()
{
	return hasScholarship;
}

void WorkingStudent::calculateScholarship()
{
	hasScholarship = calculateAvg() >= 4.5f;
}

void WorkingStudent::print()
{
	cout<<"WorkingStudent "<<name<<" "<<surname<<", aged "<<age<<", with average "<<calculateAvg()<<", working in "<<companyName<<" earns "<<salary<<"$ per month."; 
}