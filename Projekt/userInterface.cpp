#include "userInterface.h"
#include <iostream>
#include <string>
#include "exception.h"
#include "timetable.h"
using namespace std;

userInterface::userInterface(void)
{
    source = destination = fileName = "";
    destinationTime = Time(0, 0);
}

userInterface::~userInterface(void)
{
}

string userInterface::getSource() const
{
    return source;
}

string userInterface::getDestination() const
{
    return destination;
}

Time userInterface::getTime() const
{
    return destinationTime;
}

string userInterface::getFileName() const
{
    return fileName;
}

// user interaction keyboard-screen
bool userInterface::readSource()
{
    cout<<"Source: ";
    getline(cin, source);
    if (source.empty())
        return 0;

    return 1;
}

bool userInterface::readDestination()
{
    cout<<"Destination: ";
    getline(cin, destination);
    if (destination.empty())
        return 0;

    return 1;
}

bool userInterface::readTime()
{
    cout<<"Destination Time (hh:mm): ";
    try {
        cin>>destinationTime;
        cin.ignore();
    }
    catch (Exception e)
    {
        throw e;
    }

    return 1;
}

bool userInterface::readFileName()
{
    cout<<"File Name: ";
    getline(cin, fileName);
    if (fileName.empty())
        return 0;

    return 1;
}

bool userInterface::readAllData(const timetable &table)
{
    if (!readSource() || !table.sourceExist(source))
        throw Exception("Source is empty or does not exist!");
    
    if (!readDestination() || !table.destinationExist(destination))
        throw Exception("Destination is empty or does not exist!");

    readTime();

    return 1;
}