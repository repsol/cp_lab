#include "time.h"
#include "exception.h"
#include <iomanip>

Time::Time(void)
{
    hour = minutes = 0;
}

Time::~Time(void)
{
}

Time::Time(int h, int m)
{
    if (h < 0 || m < 0)
        throw Exception("Wrong Time!");

    hour = (m / 60 + h) % 24;
    minutes = m % 60;
}

int Time::getHour() const
{
    return hour;
}

int Time::getMinutes() const
{
    return minutes;
}

void Time::setHour(const int h)
{
    if (h < 0)
        throw Exception("Wrong Time!");

    hour = h % 24;
}

void Time::setMinutes(const int m)
{
    if (m < 0)
        throw Exception("Wrong Time!");

    minutes = m % 60;
}

int Time::toMinutes() const
{
    return hour*60 + minutes;
}

const Time Time::operator+ (const int min) const
{
    Time result(*this);

    result.hour += min / 60;
    result.minutes += min % 60;
    if (result.minutes >= 60)
    {
        ++result.hour;
        result.minutes -= 60;
    }
    result.hour = result.hour % 24;

    return result;
}

const Time Time::operator+ (const Time &t) const
{
    Time result(*this);

    result.hour += t.hour;
    result.minutes += t.minutes;
    if (result.minutes >= 60)
    {
        ++result.hour;
        result.minutes -= 60;
    }
    result.hour = result.hour % 24;

    return result;
}

const Time Time::operator- (const int min) const
{
    Time result(*this);

    result.hour -= min / 60;
    result.minutes -= min % 60;
    if (result.minutes < 0)
    {
        --result.hour;
        result.minutes += 60;
    }
    result.hour = result.hour % 24;
    if (result.hour < 0)
        result.hour += 24;

    return result;
}

const Time Time::operator- (const Time &t) const
{
    Time result(*this);

    result.hour -= t.hour;
    result.minutes -= t.minutes;
    if (result.minutes < 0)
    {
        --result.hour;
        result.minutes += 60;
    }
    result.hour = result.hour % 24;
    if (result.hour < 0)
        result.hour += 24;

    return result;
}

bool Time::operator< (const Time &t) const
{
    return (hour == t.hour ? minutes < t.minutes : hour < t.hour);
}

bool Time::operator<= (const Time &t) const
{
    return (hour == t.hour ? minutes <= t.minutes : hour <= t.hour);
}

std::ostream & operator<< (std::ostream &s, const Time &t)
{
    char tmp = s.fill('0');
    s<<t.hour<<":"<<std::setw(2)<<t.minutes;
    s.fill(tmp);
    return s;
}

std::istream & operator>> (std::istream &s, Time &t)
{
    char c;
    int h, m;
    s>>h>>c>>m; //hh:mm
    if (h < 0 || m < 0)
        throw Exception("Wrong Time!");
    t.hour = h;
    t.minutes = m;
    return s;
}