#include "exception.h"

Exception::Exception(void)
{
    message = "OK";
}

Exception::Exception(std::string msg) : message(msg)
{
}

Exception::~Exception(void)
{
}

void Exception::setMessage(const std::string msg)
{
    message = msg;
}

std::string Exception::getMessage() const
{
    return message;
}