#pragma once
#include <string>

class Exception
{
    std::string message;
public:
    Exception(void);
    Exception(std::string msg);
    ~Exception(void);

    void setMessage(const std::string msg);
    std::string getMessage() const;
};
