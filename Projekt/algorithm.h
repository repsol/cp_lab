#pragma once
#include "timetable.h"
#include "route.h"
#include "time.h"
#include <string>
#include <iostream>

class algorithm
{
    timetable table;
    list<route> routes;
    route result;
    std::string source, destination;
    Time destinationTime;

public:
    algorithm(void);
    algorithm(timetable t);
    ~algorithm(void);
    timetable getTimetable() const;
    void setTimetable(timetable t);
    route getResult() const;

    bool exec(const std::string s, const std::string d, const Time h);
    void printResult(std::ostream &) const;

private:
    bool init(const std::string s, const std::string d, const Time h);
    bool findAllWays();
    bool findWaysFitToTime();
    bool findOptimalWay();
    bool chooseBestWay();
};

