#include "timetable.h"
#include "exception.h"
#include <fstream>

timetable::timetable(void) : fileName("noName.txt")
{
}

timetable::timetable(string name) : fileName(name)
{
}

timetable::~timetable(void)
{
}

string timetable::getFileName() const
{
    return fileName;
}

void timetable::setFileName(string name)
{
    fileName = name;
}

bool timetable::loadFromFile()
{
    ifstream fstr;
    string source, destination;
    int hour, minutes;
    char c; // separator
    int duration;

    if (fileName.empty())
        throw Exception("File name is empty!");

    fstr.open(fileName, ios::in);
    if (!fstr.is_open())
        throw Exception("Cannot open file: "+fileName);

    while (!fstr.eof()) {

    getline(fstr, source);
    getline(fstr, destination);
    fstr>>hour>>c>>minutes; // hh:mm
    fstr>>duration;
    fstr.ignore(); // remove new line from buffer

    if (source == "EOF") // end of data
        break;

    if (source.empty() || destination.empty())
    {
        fstr.close();
        throw Exception("Source or Destination is empty!");
    }

    table.push_back(connection(source, destination, Time(hour, minutes), duration));

    } // while (!fstr.eof())

    fstr.close();
    return 1;
}

bool timetable::loadFromFile(string name)
{
    fileName = name;
    return loadFromFile();
}

bool timetable::saveToFile()
{
    ofstream fstr;

    if (fileName.empty())
        throw Exception("File name is empty!");

    fstr.open(fileName, ios::out);
    if (!fstr.is_open())
        throw Exception("Cannot open file: "+fileName);

    for(list<connection>::iterator itr = table.begin(); itr != table.end(); ++itr)
    {
        fstr<<itr->getSource()<<"\n"<<itr->getDestination()<<"\n"
            <<itr->getDeparture()<<"\n"<<itr->getDuration()<<"\n";
    }
    fstr<<"EOF";

    fstr.close();
    return 1;
}

bool timetable::saveToFile(string name)
{
    fileName = name;
    return saveToFile();
}

list<connection>::iterator timetable::begin()
{
    return table.begin();
}

list<connection>::iterator timetable::end()
{
    return table.end();
}

list<connection>::const_iterator timetable::cbegin() const
{
    return table.cbegin();
}

list<connection>::const_iterator timetable::cend() const
{
    return table.cend();
}

bool timetable::sourceExist(string search) const
{
    for(list<connection>::const_iterator itr = table.cbegin(); itr != table.cend(); ++itr)
        if (search == itr->getSource())
            return 1;

    return 0;
}

bool timetable::destinationExist(string search) const
{
    for(list<connection>::const_iterator itr = table.cbegin(); itr != table.cend(); ++itr)
        if (search == itr->getDestination())
            return 1;

    return 0;
}

void timetable::printAll(ostream &str) const
{
    str<<"Timetable.\n\n";
    for(list<connection>::const_iterator itr = table.cbegin(); itr != table.cend(); ++itr)
        str<<"connection: "<<(*itr);
    str<<endl;
}