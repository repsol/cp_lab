#pragma once
#include <iostream>
#include <string>
#include "time.h"

class connection
{
protected:
    std::string source, destination;
    int duration;
    Time departure;

public:
    connection(void);
    ~connection(void);
    connection(std::string, std::string, Time, int);
    
    std::string getSource() const;
    std::string getDestination() const;
    int getDuration() const;
    Time getDeparture() const;
    void setSource(const std::string);
    void setDestination(const std::string);
    void setDuration(const int);
    void setDeparture(const Time);

    void print(std::ostream &) const;
    friend std::ostream & operator<< (std::ostream &, const connection &);
};