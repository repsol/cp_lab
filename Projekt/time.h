#pragma once
#include <iostream>

class Time
{
    int hour, minutes;

public:
    Time(void);
    ~Time(void);
    Time(int, int);

    int getHour() const;
    int getMinutes() const;
    void setHour(const int);
    void setMinutes(const int);

    int toMinutes() const;
    const Time operator+ (const int) const;
    const Time operator+ (const Time &) const;
    const Time operator- (const int) const;
    const Time operator- (const Time &) const;
    bool operator< (const Time &) const;
    bool operator<= (const Time &) const;

    friend std::ostream & operator<< (std::ostream &, const Time &);
    friend std::istream & operator>> (std::istream &, Time &);
};