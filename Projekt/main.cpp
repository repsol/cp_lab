#include <iostream>
#include "timetable.h"
#include "algorithm.h"
#include "userInterface.h"
#include "exception.h"
using namespace std;

int main(int argc, char **argv)
{
    userInterface ui;
    timetable Timetable;
    algorithm Algorithm;

    try {
    // load timetable
        Timetable.loadFromFile("timetable.txt");

    // print timetable
        //Timetable.printAll(cout);

    // read data
        ui.readAllData(Timetable);

    // run algorithm
        Algorithm.setTimetable(Timetable);
        Algorithm.exec(ui.getSource(), ui.getDestination(), ui.getTime());
    }
    catch (Exception e)
    {
        cerr<<e.getMessage()<<endl;
        system("pause");
        return 1;
    }
    catch (...)
    {
        cerr<<"Unknown error!\n"<<endl;
        system("pause");
        return 1;
    }

    // get result
    cout<<"\nSolution:\n";
    Algorithm.printResult(cout);

    system("pause");
    return 0;
}