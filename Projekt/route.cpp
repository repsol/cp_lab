#include "route.h"
#include <iostream>
#include <list>

route::route(void) : connection()
{
    connections.clear();
}

route::~route(void)
{
}

route::route(connection p) : connection(p)
{
    connections.push_back(p);
}

const std::list<connection> * route::getConnections() const
{
    return &connections;
}

bool route::isEmpty() const
{
    return connections.empty();
}

route route::operator+ (connection &p) const
{
    route x(*this);

    x.source = p.getSource();
    x.duration += (getDeparture() - p.getDeparture()).toMinutes();
    x.departure = p.getDeparture();
    x.connections.push_front(p);

    return x;
}

void route::print(std::ostream &s) const
{
    connection::print(s);
    if (connections.size() > 1)
    {
        s<<"\nDetails:\n";
        for (std::list<connection>::const_iterator iter = connections.begin(); iter != connections.end(); ++iter)
            s<<(*iter);
    }
}