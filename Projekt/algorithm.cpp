#include "algorithm.h"
#include "exception.h"
#include <iostream>
using namespace std;

algorithm::algorithm(void)
{
}

algorithm::algorithm(timetable t)
{
    table = t;
}

algorithm::~algorithm(void)
{
}

timetable algorithm::getTimetable() const
{
    return table;
}

void algorithm::setTimetable(timetable t)
{
    table = t;
}

route algorithm::getResult() const
{
    return result;
}

bool algorithm::init(const string s, const string d, const Time h)
{
    if (!table.sourceExist(s))
        throw Exception("Algorithm: init source does not exist!");
    
    if (!table.destinationExist(d))
        throw Exception("Algorithm: init destination does not exist!");

    source = s;
    destination = d;
    destinationTime = h;
    list<connection>::iterator iterTable;

    for (iterTable = table.begin(); iterTable != table.end(); ++iterTable)
        if (iterTable->getDestination() == destination)
            routes.push_back(route(*iterTable));

    if (routes.size() > 0)
        return 1;
    else
        return 0;
}

bool algorithm::findAllWays()
{
    list<connection>::iterator iterTable;
    list<route>::iterator iterRoutes;

    iterRoutes = routes.begin();
    while(iterRoutes != routes.end())
    {
        if (iterRoutes->getSource() == source)
        {
            ++iterRoutes;
            continue;
        }
        for (iterTable = table.begin(); iterTable != table.end(); ++iterTable)
            if (iterTable->getDestination() == iterRoutes->getSource() && iterTable->getDeparture() + iterTable->getDuration() <= iterRoutes->getDeparture())
                routes.push_back(route((*iterRoutes)+(*iterTable)));

        iterRoutes = routes.erase(iterRoutes);
    }

    if (routes.size() > 0)
        return 1;
    else
        return 0;
}

bool algorithm::findWaysFitToTime()
{
    list<route>::iterator iterRoutes;

    iterRoutes = routes.begin();
    while(iterRoutes != routes.end())
    {
        if (destinationTime < iterRoutes->getDeparture() + iterRoutes->getDuration())
            iterRoutes = routes.erase(iterRoutes);
        else
            ++iterRoutes;
    }

    if (routes.size() > 0)
        return 1;
    else
        return 0;
}

bool algorithm::findOptimalWay()
{
    list<route>::iterator iterRoutes;
    int bestTime = 1000000000;

    // find best time
    for (iterRoutes = routes.begin(); iterRoutes != routes.end(); ++iterRoutes)
        if (iterRoutes->getDuration() < bestTime)
            bestTime = iterRoutes->getDuration();

    // delete non-optimal ways
    iterRoutes = routes.begin();
    while(iterRoutes != routes.end())
    {
        if (iterRoutes->getDuration() != bestTime)
            iterRoutes = routes.erase(iterRoutes);
        else
            ++iterRoutes;
    }

    if (routes.size() > 0)
        return 1;
    else
        return 0;
}

bool algorithm::chooseBestWay()
{
    list<route>::iterator iterRoutes;
    int timeDistance;
    int bestTime = 1000000000; // time distance

    iterRoutes = routes.begin();
    while(iterRoutes != routes.end())
    {
        // difference between real destinationTime and assumed one
        timeDistance = destinationTime.toMinutes() - (iterRoutes->getDeparture().toMinutes() + iterRoutes->getDuration());
        if (timeDistance < bestTime)
        {
            bestTime = timeDistance;
            result = *iterRoutes;
        }
        ++iterRoutes;
    }

    if (result.isEmpty())
        return 0;
    else
        return 1;
}

bool algorithm::exec(const std::string s, const std::string d, const Time h)
{
    if (!init(s, d, h))
        return 0;
    if (!findAllWays())
        return 0;
    if (!findWaysFitToTime())
        return 0;
    if (!findOptimalWay())
        return 0;
    if (!chooseBestWay())
        return 0;

    return 1;
}

void algorithm::printResult(std::ostream &s) const
{
    if (result.isEmpty())
        s<<"There is no route which satisfying all conditions!\n";
    else
        result.print(s);
}