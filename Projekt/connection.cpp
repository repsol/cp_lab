#include "connection.h"
#include "exception.h"

connection::connection(void)
{
    source = destination = "";
    departure = Time(0, 0);
    duration = 0;
}

connection::~connection(void)
{
}

connection::connection(std::string s, std::string d, Time dep, int dur)
{
    if (dur < 0)
        throw Exception("Wrong duration!");

    source = s;
    destination = d;
    departure = dep;
    duration = dur;
}

std::string connection::getSource() const
{
    return source;
}

std::string connection::getDestination() const
{
    return destination;
}

int connection::getDuration() const
{
    return duration;
}

Time connection::getDeparture() const
{
    return departure;
}

void connection::setSource(const std::string s)
{
    source = s;
}

void connection::setDestination(const std::string d)
{
    destination = d;
}

void connection::setDuration(const int dur)
{
    if (dur < 0)
        throw Exception("Wrong duration!");

    duration = dur;
}

void connection::setDeparture(const Time dep)
{
    departure = dep;
}

void connection::print(std::ostream &s) const
{
    s<<source<<" -> "<<destination<<"\ndeparture time: "<<departure<<"\tduration: "<<duration<<"\tat destination: "<<departure+duration<<std::endl;
}

std::ostream & operator<< (std::ostream &s, const connection &p)
{
    p.print(s);
    return s;
}