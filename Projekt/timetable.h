#pragma once
#include "connection.h"
#include <iostream>
#include <string>
#include <list>
using namespace std;

class timetable
{
    list<connection> table;
    string fileName;

public:
    timetable(void);
    timetable(string);
    ~timetable(void);
    string getFileName() const;
    void setFileName(string);
    bool loadFromFile();
    bool loadFromFile(string);
    bool saveToFile(); // check fileName not empty
    bool saveToFile(string);
    list<connection>::iterator begin();
    list<connection>::iterator end();
    list<connection>::const_iterator cbegin() const;
    list<connection>::const_iterator cend() const;
    bool sourceExist(string) const;
    bool destinationExist(string) const;
    void printAll(ostream &) const;
};

