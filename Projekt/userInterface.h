#pragma once
#include <string>
#include "time.h"
#include "timetable.h"

class userInterface
{
    std::string source, destination;
    Time destinationTime;
    std::string fileName;

public:
    userInterface(void);
    ~userInterface(void);

    // getters
    std::string getSource() const;
    std::string getDestination() const;
    Time getTime() const;
    std::string getFileName() const;

    // user interaction keyboard-screen
    bool readSource();
    bool readDestination();
    bool readTime();
    bool readFileName();
    bool readAllData(const timetable &);
};

