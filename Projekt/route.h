#pragma once
#include "connection.h"
#include <list>
#include <iostream>

class route : public connection
{
protected:
    std::list<connection> connections;

public:
    route(void);
    ~route(void);
    route(connection);
    const std::list<connection> * getConnections() const;
    bool isEmpty() const;
    route operator+ (connection &) const;
    void print(std::ostream &) const;
};