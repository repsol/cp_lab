#pragma once
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

/* Class declaration */
template<class T>
class MatrixTemp
{
	/* MatrixTemp contents */
	T **data;
	/* MatrixTemp dimensions */
	int rowCnt, colCnt;	

    /* Check type */
    bool checkType();
public:
	/* Copy constructor */
	MatrixTemp(const MatrixTemp& matrix);
	/* Overloaded assignment operator */
	MatrixTemp& operator=(const MatrixTemp& matrix);
	/* Constructor with parameters */
	MatrixTemp(T **data, int rowCnt, int colCnt);
	/* Destructor */
	~MatrixTemp();
	/* Getters & setters */
	T** getData() const { return data; }
	int getRowCnt() const { return rowCnt; }
	int getColCnt() const { return colCnt; }
	void setData(T **data, int rowCnt, int colCnt);
	void setRowCnt(int rowCnt) { this->rowCnt = rowCnt; }
	void setColCnt(int colCnt) { this->colCnt = colCnt; }

    template<class T> friend ostream& operator<< (ostream &str, const MatrixTemp<T> &m);
    template<class T> friend ofstream& operator<< (ofstream &str, const MatrixTemp<T> &m);
};

/* 
 * Copy constructor
 * @param matrix	-		reference to an existing object to be copied
 */
template<class T>
MatrixTemp<T>::MatrixTemp (const MatrixTemp<T>& matrix) : rowCnt(matrix.rowCnt), colCnt(matrix.colCnt)
{
	data = new T*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		data[i] = new T[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			data[i][j] = matrix.data[i][j];
}

/*
 * Overloaded assignment operator
 * @param matrix	-		reference to an existing object to be copied
 * @return			-		a copy of existing object
 */
template<class T>
MatrixTemp<T>& MatrixTemp<T>::operator=(const MatrixTemp<T>& matrix)
{
	if(this != &matrix)
	{
		for(int i = 0; i < rowCnt; i++)
			delete [] data[i];
		delete [] data;
		rowCnt = matrix.rowCnt;
		colCnt = matrix.colCnt;
		data = new T*[rowCnt];
		for(int i = 0; i < rowCnt; i++)
			data[i] = new T[colCnt];
		for(int i = 0; i < rowCnt; i++)
			for(int j = 0; j < colCnt; j++)
				data[i][j] = matrix.data[i][j];
	}
	return *this;
}

/*
 * Constructor with parameters
 * @param data		-		new matrix contents
 * @param rowCnt	-		number of rows in the matrix
 * @param colCnt	-		number of columns in the matrix
 */
template<class T>
MatrixTemp<T>::MatrixTemp(T **data, int rowCnt, int colCnt) : rowCnt(rowCnt), colCnt(colCnt)
{
    if(!checkType())
        throw -1;

	this->data = new T*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		this->data[i] = new T[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			this->data[i][j] = data[i][j];
}

/* Class destructor */
template<class T>
MatrixTemp<T>::~MatrixTemp()
{
	for(int i = 0; i < rowCnt; i++)
		delete [] data[i];
	delete [] data;
}

/*
 * MatrixTemp contents setter
 * @param data		-		new matrix contents
 * @param rowCnt	-		number of rows in the matrix
 * @param colCnt	-		number of columns in the matrix
 */
template<class T>
void MatrixTemp<T>::setData(T **data, int rowCnt, int colCnt)
{
	for(int i = 0; i < this->rowCnt; i++)
		delete [] this->data[i];
	delete [] this->data;
	this->rowCnt = rowCnt;
	this->colCnt = colCnt;
	this->data = new T*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		this->data[i] = new T[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			this->data[i][j] = data[i][j];
}

template<class T>
bool MatrixTemp<T>::checkType()
{
    if (typeid(T) == typeid(int))
        return true;
    if (typeid(T) == typeid(float))
        return true;
    if (typeid(T) == typeid(double))
        return true;

    return false;
}

template<class T>
ostream& operator<< (ostream& str, const MatrixTemp<T> &m)
{
    for(int i = 0; i < m.rowCnt; i++)
    {
        str<<"[ ";
        for(int j = 0; j < m.colCnt; j++)
        {
            str<<fixed<<setprecision(2)<<setw(5)<<(m.getData())[i][j]<<'\t';
        }
        str<<"]\n";
    }
    return str;
}

template<class T>
ofstream& operator<< (ofstream &str, const MatrixTemp<T> &m)
{
    for(int i = 0; i < m.rowCnt; i++)
    {
        for(int j = 0; j < m.colCnt; j++)
        {
            str<<scientific<<setprecision(3)<<showpos<<(m.getData())[i][j]<<' ';
        }
        str<<endl;
    }
    return str;
}