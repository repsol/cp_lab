#include "common_headers.h"

/* 
 * Copy constructor
 * @param matrix	-		reference to an existing object to be copied
 */
Matrix::Matrix(const Matrix& matrix) : rowCnt(matrix.rowCnt), colCnt(matrix.colCnt)
{
	data = new int*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		data[i] = new int[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			data[i][j] = matrix.data[i][j];
}

/*
 * Overloaded assignment operator
 * @param matrix	-		reference to an existing object to be copied
 * @return			-		a copy of existing object
 */
Matrix& Matrix::operator=(const Matrix& matrix)
{
	if(this != &matrix)
	{
		for(int i = 0; i < rowCnt; i++)
			delete [] data[i];
		delete [] data;
		rowCnt = matrix.rowCnt;
		colCnt = matrix.colCnt;
		data = new int*[rowCnt];
		for(int i = 0; i < rowCnt; i++)
			data[i] = new int[colCnt];
		for(int i = 0; i < rowCnt; i++)
			for(int j = 0; j < colCnt; j++)
				data[i][j] = matrix.data[i][j];
	}
	return *this;
}

/*
 * Constructor with parameters
 * @param data		-		new matrix contents
 * @param rowCnt	-		number of rows in the matrix
 * @param colCnt	-		number of columns in the matrix
 */
Matrix::Matrix(int **data, int rowCnt, int colCnt) : rowCnt(rowCnt), colCnt(colCnt)
{
	this->data = new int*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		this->data[i] = new int[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			this->data[i][j] = data[i][j];
}

/* Class destructor */
Matrix::~Matrix()
{
	for(int i = 0; i < rowCnt; i++)
		delete [] data[i];
	delete [] data;
	
}

/*
 * Matrix contents setter
 * @param data		-		new matrix contents
 * @param rowCnt	-		number of rows in the matrix
 * @param colCnt	-		number of columns in the matrix
 */
void Matrix::setData(int **data, int rowCnt, int colCnt)
{
	for(int i = 0; i < this->rowCnt; i++)
		delete [] this->data[i];
	delete [] this->data;
	this->rowCnt = rowCnt;
	this->colCnt = colCnt;
	this->data = new int*[rowCnt];
	for(int i = 0; i < rowCnt; i++)
		this->data[i] = new int[colCnt];
	for(int i = 0; i < rowCnt; i++)
		for(int j = 0; j < colCnt; j++)
			this->data[i][j] = data[i][j];
}