#pragma once

/* Class declaration */
class Matrix
{
	/* Matrix contents */
	int **data;
	/* Matrix dimensions */
	int rowCnt, colCnt;	
public:
	/* Copy constructor */
	Matrix(const Matrix& matrix);
	/* Overloaded assignment operator */
	Matrix& operator=(const Matrix& matrix);
	/* Constructor with parameters */
	Matrix(int **data, int rowCnt, int colCnt);
	/* Destructor */
	~Matrix();
	/* Getters & setters */
	int** getData() { return data; }
	int getRowCnt() { return rowCnt; }
	int getColCnt() { return colCnt; }
	void setData(int **data, int rowCnt, int colCnt);
	void setRowCnt(int rowCnt) { this->rowCnt = rowCnt; }
	void setColCnt(int colCnt) { this->colCnt = colCnt; }	
};