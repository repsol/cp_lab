#include "common_headers.h"

using namespace std;

int main(int argc, char* argv[])
{
	/* Exemplary matrix sizes */
	const int ROWS = 3, COLS = 5;

	/* Pointers to template classes */
	MatrixTemp<int> *m1;
	MatrixTemp<char> *m2;
	MatrixTemp<float> *m3;
	/* 
	 * Memory & contents allocations for 
	 * respective template contents
	 */
	int **x1 = new int*[ROWS];
	char **x2 = new char*[ROWS];
	float **x3 = new float*[ROWS];
	for(int i = 0; i < ROWS; i++)
	{
		x1[i] = new int[COLS];
		x2[i] = new char[COLS];
		x3[i] = new float[COLS];
	}
	for(int i = 0; i < ROWS; i++)
	{
		for(int j = 0; j < COLS; j++)
		{
			x1[i][j] = i * i - 5 * j;
			x2[i][j] = (char) (i + j - 7);
			x3[i][j] = i * j - 3.5f * i + 1.5f * j;
		}
	}
	/*
	 * Creation of class objects - surrounded
	 * with try and catch to deal with improper
	 * type exceptions
	 */
	try {
		m1 = new MatrixTemp<int>(x1, ROWS, COLS);
	}
	catch(int) 
	{ 
		cerr << "Error --> Improper type: int" << endl;
		m1 = NULL;
	}
	try {
		m2 = new MatrixTemp<char>(x2, ROWS, COLS);	
	}
	catch(int) 
	{ 
		cerr << "Error --> Improper type: char" << endl;
		m2 = NULL;
	}
	try {
		m3 = new MatrixTemp<float>(x3, ROWS, COLS);
	}
	catch(int) 
	{ 
		cerr << "Error --> Improper type: float" << endl;
		m3 = NULL;
	}		
	
	/* Use of overloaded output streams */
	ofstream o("test.txt");		
	cout << *m3;
    o << *m3;
	o.close();		
	
	/* Memory deallocations */
	for(int i = 0; i < ROWS; i++)
	{
		delete [] x1[i];
		delete [] x2[i];
		delete [] x3[i];
	}
	delete [] x1;
	delete [] x2;
	delete [] x3;	
	delete m1;
	delete m2;
	delete m3;

    system("pause");
	return 0;
}