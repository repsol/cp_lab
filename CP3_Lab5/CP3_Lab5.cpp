#include "common_headers.h"

using namespace std;

int main(int argc, char* argv[])
{
	/* Initializing random generator seed */
	srand(time(NULL));
	/* 
	 * Use these constants to specify the count
	 * of _3dPoints and _3DVectors
	 */
	const int PT_SIZE = 10;
	const int V_SIZE = PT_SIZE / 2;
	/*
	 * Declare vector and deque containers
	 * for appropriate objects, with selected
	 * template types, e.g. int, float, etc.
	 * Declare also deque iterators (forward and reverse).
	 *
	 * Note: While using double '>' sign there has to be
	 * a space between them, i.e. not >> but > >, not to
	 * confuse the stream operator with container or
	 * iterator declaration.
	 */
	
    vector<_3DPoint<float> > vec;
    deque<_3DVector<float> > deq;
    deque<_3DVector<float> >::iterator iter;
    deque<_3DVector<float> >::reverse_iterator riter;

	/*	 
	 * Insert _3DPoint objects into a vector
	 * using push_back(...) method
	 */		
	for(int i = 0; i < PT_SIZE; i++)
    {
        _3DPoint<float> p((rand()%10000)/100.0f, (rand()%10000)/100.0f, (rand()%10000)/100.0f);
        vec.push_back(p);
    }
	
	/* 
	 * Insert _3DVector objects into a deque
	 * using push_back(...) method
	 */		
	for(int i = 0; i < V_SIZE; i++)
    {
        _3DVector<float> v(&vec[i], &vec[V_SIZE+i]);
        deq.push_back(v);
    }
		
	cout << "UnSorted: " << endl;
	
	/* 
	 * Iterate through deque and print contents
	 * using iterator dereferencing and stream operator.
	 */
	for(iter=deq.begin(); iter!=deq.end(); iter++)
    {
        cout<<*iter<<endl;
    }
	
	/* Sort deque contents using either predicate or function object */
	sort(deq.begin(), deq.end(), comp<float>);	
	
	cout << endl << "Sorted: " << endl;
	
	/* 
	 * Iterate through deque and print contents
	 * using iterator dereferencing and stream operator.
	 */	
	for(iter=deq.begin(); iter!=deq.end(); iter++)
    {
        cout<<*iter<<endl;
    }
	
	/* Shuffle container contents */
	random_shuffle(deq.begin(), deq.end());
	
	cout << endl << "UnSorted again: " << endl;
	
	/* 
	 * Iterate through deque and print contents
	 * using iterator dereferencing and stream operator.
	 */	
	for(iter=deq.begin(); iter!=deq.end(); iter++)
    {
        cout<<*iter<<endl;
    }
	
	/* 
	 * Sort deque contents using either predicate or function object
	 * - use the method different from the previous case
	 */
	sort(deq.begin(), deq.end(), Comparator<float>());	
	
	cout << endl << "Sorted again, but shown in reverse order: " << endl;

	/* 
	 * Iterate BACKWARD through deque and print contents
	 * using iterator dereferencing and stream operator.
	 */	
	for(riter=deq.rbegin(); riter!=deq.rend(); riter++)
    {
        cout<<*riter<<endl;
    }

    system("pause");
	return 0;
}