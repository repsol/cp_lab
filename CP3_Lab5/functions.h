#pragma once
#include "_3DVector.h"

/* function: compare two vectos */
template<class T>
bool comp(const _3DVector<T> &v1, const _3DVector<T> &v2)
{
    return v1.norm() < v2.norm();
}

/* comparator class */
template<class T>
class Comparator
{
public:
    bool operator() (const _3DVector<T> &v1, const _3DVector<T> &v2)
    {
        return v1.norm() < v2.norm();
    }
};
