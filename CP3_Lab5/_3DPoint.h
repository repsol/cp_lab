#pragma once
#include <iostream>
#include <iomanip>

template<class T>
class _3DPoint
{
    /* coordinates */
    T x, y, z;

public:
    /* constructor */
    _3DPoint(T x, T y, T z): x(x), y(y), z(z)
    {}

    /* getters */
    T getX() const { return x; }
    T getY() const { return y; }
    T getZ() const { return z; }
    /* setters */
    void setX(T x) { this->x = x; }
    void setY(T y) { this->x = y; }
    void setZ(T z) { this->x = z; }
    /* overloaded operator << */
    template<class T> friend std::ostream& operator<< (std::ostream &s, _3DPoint<T> &p);
};

template<class T>
std::ostream& operator<< (std::ostream &s, _3DPoint<T> &p)
{
    s<<fixed<<setprecision(2)<<"[ "<<setw(6)<<p.x<<", "<<setw(6)<<p.y<<", "<<setw(6)<<p.z<<" ]";
    return s;
}
