#pragma once
#include "_3DPoint.h"
#include <iostream>

template<class T>
class _3DVector
{
    /* coordinates of vector */
    _3DPoint<T> *start, *end;

public:
    /* constructor */
    _3DVector(_3DPoint<T> *s, _3DPoint<T> *e);
    /* copy constructor */
    _3DVector(const _3DVector<T> &v);
    /* destructor */
    ~_3DVector();
    /* assign operator */
    _3DVector<T>& operator=(const _3DVector<T> &v);
    /* calculate squared norm of vector */
    T norm () const;
    /* overloaded operator << */
    template<class T> friend std::ostream& operator<< (std::ostream &s, _3DVector<T> &v);
};

template<class T>
_3DVector<T>::_3DVector(_3DPoint<T> *s, _3DPoint<T> *e)
{
    start = new _3DPoint<T>(*s);
    end = new _3DPoint<T>(*e);
}

template<class T>
_3DVector<T>::_3DVector(const _3DVector<T> &v)
{
    start = new _3DPoint<T>(*v.start);
    end = new _3DPoint<T>(*v.end);
}

template<class T>
_3DVector<T>::~_3DVector()
{
    delete start;
    delete end;
}

template<class T>
_3DVector<T>& _3DVector<T>::operator=(const _3DVector<T> &v)
{
    if (this == &v)
        return *this;

    delete start;
    delete end;
    start = new _3DPoint<T>(*v.start);
    end = new _3DPoint<T>(*v.end);
    return *this;
}

template<class T>
T _3DVector<T>::norm() const
{
    return (end->getX() - start->getX())*(end->getX() - start->getX()) + (end->getY() - start->getY())*(end->getY() - start->getY()) + 
        (end->getZ() - start->getZ())*(end->getZ() - start->getZ());
}

template<class T>
std::ostream& operator<< (std::ostream &s, _3DVector<T> &v)
{
    s<<"Start point: "<<*v.start<<"\n End point:  "<<*v.end;
    s<<"\nNorm:\t"<<v.norm();
    return s;
}
